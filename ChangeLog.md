#CHANGELOG for BrowserChoice

**Latest version**: 1.2.2

## 4/6/2017 - Version 1.2.2
- Started to write this changelog ;)
- Added an option to check of newer version. You can disable this in the app.config. A check won't happen at every launch time, just once every 6hours
- It's not possible to switch to the default url by using a shortcut key (by default `d`)
- Altered the setup project to add proper shortcuts and check if an app.config already exists, so that I'm not overwriting an existing file
- Altered the post-build events to do a signing of the binaries in the \obj\ and \bin\ folder