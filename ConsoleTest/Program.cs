﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Win32;

namespace ConsoleTest
{
    class Program
    {
        static void Main(string[] args)
        {
            Dictionary<string, string> browserList = new Dictionary<string, string>();

            Console.WriteLine("Here's the list of installed browsers:");

            browserList = InstalledBrowsers();

            foreach (var browser in browserList.Keys)
            {
                Console.WriteLine("Browser {0} with CmdKey {1}", browser, browserList[browser]);
            }

            Console.ReadLine();

        }

        public static Dictionary<string, string> InstalledBrowsers()
        {
            Dictionary<string, string> browserList = new Dictionary<string, string>();

            RegistryKey browserKeys;

            browserKeys = Registry.LocalMachine.OpenSubKey(@"SOFTWARE\WOW6432Node\Clients\StartMenuInternet");

            if (browserKeys == null)
            {
                browserKeys = Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Clients\StartMenuInternet");
            }

            string[] browserNames = browserKeys.GetSubKeyNames();

            foreach (string browser in browserNames)
            {
                using (RegistryKey tempKey = browserKeys.OpenSubKey(browser))
                {
                    try
                    {
                        string cmdKey = tempKey.OpenSubKey("shell\\open\\command").GetValue(null).ToString();

                        switch (tempKey.GetValue(null).ToString())
                        {

                            case "Internet Explorer":
                                browserList.Add("IE", cmdKey);
                                break;
                            case "Google Chrome":
                                browserList.Add("Chrome", cmdKey);
                                break;
                            case "Mozilla Firefox":
                                browserList.Add("FF", cmdKey);
                                break;
                            default:
                                break;
                        }
                    }
                    catch
                    {
                        //throw;
                    }
                }
            }
            return browserList;
        }
    }
}
