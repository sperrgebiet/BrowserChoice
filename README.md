# Introduction
Do you have multipel browsers installed and have to copy and paste URLs as you want to open some websites with a specific browser, but other URLs with another browser? Like using Chrome for private use, and Edge for everything business related?
If so, BrowserChoice is here to make your life much easier.

Once BrowserChoice is set as default browser you can choose which browser you want to open it with for every link you open in another application or associated files on your machine.

# Getting Started

### 1. Download
- If you read this as part of the help file you apparently already installed the tool ;)
- Otherwise just open the [wiki](https://gitlab.com/sperrgebiet/BrowserChoice/wikis/) to find more information, including the download link.
- Direct download: [https://gitlab.com/sperrgebiet/BrowserChoice/blob/master/Setup/Release/BrowserChoice.msi](https://gitlab.com/sperrgebiet/BrowserChoice/blob/master/Setup/Release/BrowserChoice.msi)

### 2. Installation
Install the package as usual. After the installation has finished the Control Panel opens to set BrowserChoice as default browser.
Once that is done BrowserChoice will automatically launch when you open a link or a file with an associated file extension.
### 3. Software dependencies
.Net Framework 4.5

### 4. Latest releases
1.2.2

# Usage

## Features
- Acts as 'default browser' and gives you the selection to open this URL with the selected browser
- If there is a valid URL in the OS clipboard you can open that URL directly with the selected browser
- Launch the binary directly with an URL as parameter
- Checks if the provided URL (as parameter or clipboard) is a valid URI format
- Always open a default URL, regardless of the parameter or clipboard content (Keyboard shortcut `d`, can be changed in the config file
- Update notification when a new build is available

## Association
If you set BrowserChoice as default browser it will associate itself with the following protocols and file extensions:

- HTTP
- HTTPS
- FTP
- *.HTM
- *.HTML
- *.SHTML
- *.XHT
- *.XHTML

## URL validation
There is the following logic to provide a URL to the selected Browser:

1. A valid string is provided as parameter
2. A valid string is in the clipboard
3. A valid URL is specified in the config file.

A valid string is a pretty generic term, but BrowserChoice will just try to create a somehow valid URL from pretty much every string it gets. Like from `foo` it will create `http://foo`
You might think this is wrong/a bug/insane. But as there are not just URLs in the Internet but also in Intranets it's possible to use short names. Hence I tried to keep it as flexible, and hence also kinda dump as possible.

## Keyboard shortcuts
In the main window you can also use keyboard shortcuts to launch a particular browser or to enable InPrivate/Incognito mode. By default there are the following shortcuts available:

- `1` -> Launch Google Chrome
- `2` -> Launch Mozilla Firefox
- `3` -> Launch Microsoft Internet Explorer
- `4` -> Launch Microsoft Edge
- `q` -> Toggle the InPrivate checkbox
- `d` -> Toggles between the provided URL (parameter or clipboard) and the defualt URL

> You can customize the shortcuts within the app config file.

## Visible URL
To give you a better understand which URL will be opened you can see the one which gets passed to the browser in a little label at the lower part of the app. If the URL is too long and you want to double check it (or for troubleshooting purposes) you can simply right click on the URL and it will get copied to the clipboard.

# Customization

## App Config
There are a couple of things you can configure within BrowserChoice. You can find all available options within the app.config. The `BrowserChoice.exe.config` file is within the installation directory (by default: `C:\Program Files (x86)\sperrNET\BrowserChoice`)

The default config file looks like the following:

	<?xml version="1.0" encoding="utf-8"?>
	<configuration>|
	  <startup>
	    <supportedRuntime version="v4.0" sku=".NETFramework,Version=v4.5.2" />
	  </startup>
	  <appSettings>
	    <add key="DefaultUrl" value="https://www.sperrgebiet.org" />
	    <add key="WindowTitle" value="Where do you want to go today?" />
	    <add key="ChromeShortcut" value="1" />
	    <add key="FFShortcut" value="2" />
	    <add key="IEShortcut" value="3" />
	    <add key="EdgeShortcut" value="4" />
	    <add key="InPrivateShortcut" value="q" />
	    <add key="ToggleUseDefaultUrl" value="d" />
    	<add key="UpdateCheck" value="true" />
    	<add key="UpdateUrl" value="http://update.sperrgebiet.org/BrowserChoice/latestversion.json" />
	  </appSettings>
	</configuration>

`<add key="DefaultUrl" value="https://www.sperrgebiet.org" />` If there is no valid URL in the clipboard, nor as a parameter we'll use this URL as default one


`<add key="WindowTitle" value="Where do you want to go today?" />` Just to personalize the title of the window. Be careful with special characters. If an exception is thrown, just use something normal ;) 

`<add key="ChromeShortcut" value="1" />` Defines the keyboard key which should be used to select Chrome 

`<add key="FFShortcut" value="2" />` Defines the keyboard key which should be used to select Firefox 

`<add key="IEShortcut" value="3" />` Defines the keyboard key which should be used to select Internet Explorer 

`<add key="EdgeShortcut" value="4" />` Defines the keyboard key which should be used to select Edge 

`<add key="InPrivateShortcut" value="q" />` Defines the keyboard key which should be used to toggle the InPrivate checkbox 

`<add key="ToggleUseDefaultUrl" value="d" />` Defines the keyboard key which will toggle between the provided URL, and the default URL from the config file
`<add key="UpdateCheck" value="true" />` Set it to true or false. If enabled (true) we'll check once every 6 hours if there is a new update available.
`<add key="UpdateUrl" value="http://update.sperrgebiet.org/BrowserChoice/latestversion.json" />` Please don't change this value, as it's the URL where we'll check for new updates

## Custom Icon
If you don't like the default icon of BrowserChoice simply but a valid *.ico file with the name `BrowserChoice.ico` within the installation directory. If there is no such file the default icon will be used.

> If you pin the app to the task bar keep in mind that you have to change the icon there manually!

# Parameters
So far there are just two parameters for BrowserChoice

    BrowserChoice.exe /Default
Will open the control panel to set BrowserChoice as default browser.
   

    BrowserChoice.exe /Help
Displays the help window


# Misc
- BrowserChoice should detect which browsers are available and will just enable the buttons for installed browsers.
- So far just 4 browsers are supported. Chrome, Firefox, Internet Explorer, Edge
- I'm not a Dev, so don't blame me for bad code quality :P This project started because I wanted a solution for myself, and as sharing is caring I wanted to share it with you too :)

# Known limitations
- Microsoft Edge can't be opened InPrivate from the command line yet, hence BrowserChoice can't open InPrivate sessions in Edge either.
- Microsoft Edge apparently doesn't support it to open a local file provided as parameter, hence BrowserChoice can't open local files in Edge.

# Help/Suggestions/Contact #
<a href="https://gitlab.com/sperrgebiet/BrowserChoice" target="_blank">https://gitlab.com/sperrgebiet/BrowserChoice</a> || <a href="https://www.sperrgebiet.org" target="_blank">https://www.sperrgebiet.org</a>