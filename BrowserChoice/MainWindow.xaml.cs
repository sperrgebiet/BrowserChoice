﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Diagnostics;
using Microsoft.Win32;
using System.Configuration;
using System.Windows.Shell;
using System.Windows.Threading;

namespace BrowserChoice
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            SetVariables();
            InitializeJumpList();
            GetTitleFromAppConfig();
            SetAppIcon();
            EnableAvailableBrowsers();
            CheckUpdate();
        }

        private static string url { get; set; }
        private static string tmpUrl { get; set; }              // tmpUrl is just used for the ToggleDefaultUrl method
        private static string prgPath { get; set; }
        private static string chromePath { get; set; }
        private static string iePath { get; set; }
        private static string ffPath { get; set; }
        private static string edgePath { get; set; }
        private string[] args { get; set; }
        private string cmdParams { get; set; }
        private string clipUrl { get; set; }
        private Dictionary<string, string> shortcuts = new Dictionary<string, string>();
        private Dictionary<string, string> browserList { get; set; }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            InitializeJumpList();
        }

        private void btn_Chrome_Click(object sender, RoutedEventArgs e)
        {
            StartChrome();
        }

        private void btn_FF_Click(object sender, RoutedEventArgs e)
        {
            StartFF();
        }

        private void btn_IE_Click(object sender, RoutedEventArgs e)
        {
            StartIE();
        }

        private void btn_Edge_Click(object sender, RoutedEventArgs e)
        {
            StartEdge();
        }

        private void SetVariables()
        {
            args = Environment.GetCommandLineArgs();

            //The first argument is always the current binary path
            if (args.Length == 2)
            {
                URLChoice paramUrl = CreateValidUrl(args[1]);

                if (args[1].ToString().StartsWith("/") && args[1].ToString().ToLower() == "/Default".ToLower())
                {
                    ShowDefaultProgramsUI();
                    App.Current.Shutdown();
                }
                else if (args[1].ToString().StartsWith("/") && args[1].ToString().ToLower() == "/Help".ToLower())
                {
                    App.Current.MainWindow.Topmost = false;
                    url = GetUrl();
                    ShowHelpWindow();
                }
                else if (paramUrl.urlValid || System.IO.File.Exists(paramUrl.urlString)) //We've to consider URLs as well as local file paths
                {
                    url = paramUrl.urlString;
                }
                else
                {
                    url = GetUrl();
                }
            }
            else if (args.Length == 3)
            {
                URLChoice paramUrl = CreateValidUrl(args[1] + " " + args[2]);
                if (paramUrl.urlValid)
                {
                    url = paramUrl.urlString;
                }
                else
                {
                    url = GetUrl();
                }
            }
            else
            {
                url = GetUrl();
            }

            // Define browser paths
            // Not needed anymore as we're now getting the path to the binary directly from Registry
            //prgPath = Environment.GetFolderPath(Environment.SpecialFolder.ProgramFilesX86);
            //chromePath = prgPath + "\\Google\\Chrome\\Application\\chrome.exe";
            //ffPath = prgPath + "\\Mozilla Firefox\\firefox.exe";
            //iePath = prgPath + "\\Internet Explorer\\iexplore.exe";

            // Sets the binary path based on the installed browsers from the Registry.
            // Edge will be disabled anyways when it's not OS MajorVersion 10, hence we don't care here about it
            Dictionary<string, string> browserList = new Dictionary<string, string>();
            browserList = GetInstalledBrowsers();

            foreach (string browser in browserList.Keys)
            {
                switch (browser)
                {
                    case "Chrome":
                        chromePath = browserList["Chrome"];
                        break;
                    case "FF":
                        ffPath = browserList["FF"];
                        break;
                    case "IE":
                        iePath = browserList["IE"];
                        break;
                }
            }

            edgePath = "microsoft-edge:";

            // Gets the configures shortcuts to launch a browser directly
            shortcuts.Add("Chrome", GetKeyboardShortcuts("Chrome"));
            shortcuts.Add("FF", GetKeyboardShortcuts("FF"));
            shortcuts.Add("IE", GetKeyboardShortcuts("IE"));
            shortcuts.Add("Edge", GetKeyboardShortcuts("Edge"));
            shortcuts.Add("InPrivate", GetKeyboardShortcuts("InPrivate"));
            shortcuts.Add("UseDefaultUrl", GetKeyboardShortcuts("UseDefaultUrl"));

            // Shows the current URL in the label
            this.lbl_Url.Content = url;
        }

        public void InitializeJumpList()
        {
            JumpList jList = new JumpList();

            JumpTask chromeTask = new JumpTask();
            chromeTask.Title = "Start Chrome";
            chromeTask.ApplicationPath = chromePath;
            chromeTask.Arguments = url;
            chromeTask.IconResourcePath = chromePath;

            JumpTask ffTask = new JumpTask();
            ffTask.Title = "Start Firefox";
            ffTask.ApplicationPath = ffPath;
            ffTask.Arguments = url;
            ffTask.IconResourcePath = ffPath;

            JumpTask ieTask = new JumpTask();
            ieTask.Title = "Start Internet Explorer";
            ieTask.ApplicationPath = iePath;
            ieTask.Arguments = url;
            ieTask.IconResourcePath = iePath;

            JumpTask edgeTask = new JumpTask();
            edgeTask.Title = "Start Edge";
            edgeTask.ApplicationPath = edgePath;
            edgeTask.IconResourcePath = System.IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Windows), "explorer.exe");
            edgeTask.IconResourceIndex = 77;

            // InPrivate Modes
            JumpTask chromeTaskiP = new JumpTask();
            chromeTaskiP.Title = "Start Chrome Incognito";
            chromeTaskiP.ApplicationPath = chromePath;
            chromeTaskiP.IconResourcePath = chromePath;
            chromeTaskiP.Arguments = "-incognito " + url;
            chromeTaskiP.CustomCategory = "InPrivate";


            JumpTask ffTaskiP = new JumpTask();
            ffTaskiP.Title = "Start Firefox Private Browsing";
            ffTaskiP.ApplicationPath = ffPath;
            ffTaskiP.IconResourcePath = ffPath;
            ffTaskiP.Arguments = "-private-window " + url;
            ffTaskiP.CustomCategory = "InPrivate";

            JumpTask ieTaskiP = new JumpTask();
            ieTaskiP.Title = "Start Internet Explorer InPrivate";
            ieTaskiP.ApplicationPath = iePath;
            ieTaskiP.IconResourcePath = iePath;
            ieTaskiP.Arguments = "-private " + url;
            ieTaskiP.CustomCategory = "InPrivate";

            JumpList.SetJumpList(Application.Current, jList);
            jList.JumpItems.Add(chromeTask);
            jList.JumpItems.Add(ffTask);
            jList.JumpItems.Add(ieTask);
            jList.JumpItems.Add(edgeTask);
            jList.JumpItems.Add(chromeTaskiP);
            jList.JumpItems.Add(ffTaskiP);
            jList.JumpItems.Add(ieTaskiP);
            jList.Apply();
        }

        private void StartChrome()
        {
            StartProcess(chromePath, "-incognito");
        }

        private void StartFF()
        {
            StartProcess(ffPath, "-private-window");
        }

        private void StartIE()
        {
            StartProcess(iePath, "-private");
        }
        private void StartEdge()
        {
            StartProcess(edgePath + url, "");
        }

        // The following is our fallback. In case that no valid URL is specified as parameter we'll try
        // to use the clipboard or read the URL from the config file
        private string GetUrl()
        {
            try
            {
                URLChoice clipUrl = CreateValidUrl(Clipboard.GetText());
                URLChoice configUrl = CreateValidUrl(ConfigurationManager.AppSettings["DefaultUrl"]);



                if (clipUrl.urlValid)
                {
                    return clipUrl.urlString;
                }
                else if (configUrl.urlValid)
                {
                    return configUrl.urlString;
                }
                else
                {
                    return "https://www.bing.com";
                }
            }
            catch (Exception)
            {
                return "https://www.bing.com";
            }
        }

        private URLChoice CreateValidUrl(string url)
        {
            URLChoice urlChoice = new URLChoice();
            Uri validUrl;

            try
            {
                // By using UriBuilder we can create a valid Url, even when just a part of it is specified
                // Afterwards we'll still do the check if everything looks good
                UriBuilder validUri = new UriBuilder(url);
                url = validUri.Uri.AbsoluteUri.ToString();
                Uri.TryCreate(url, UriKind.Absolute, out validUrl);
                urlChoice.urlValid = true;
                urlChoice.urlString = validUrl.AbsoluteUri.ToString();
            }
            catch (Exception)
            {
                urlChoice.urlValid = false;
            }

            return urlChoice;
        }

        private void StartProcess(string binaryPath, string inPrivateArg)
        {
            //string command = binaryPath + url;
            if (chk_Private.IsChecked.Value)
            {
                cmdParams = inPrivateArg + " " + url;
            }
            else
            {
                cmdParams = url;
            }
            Process.Start(binaryPath, cmdParams);
            App.Current.Shutdown();
        }

        private void ToggleInPrivate()
        {
            if (chk_Private.IsChecked.Value)
            {
                chk_Private.IsChecked = false;
            }
            else
            {
                chk_Private.IsChecked = true;
            }
        }

        private void ToggleUseDefaultUrl()
        {
            URLChoice configUrl = CreateValidUrl(ConfigurationManager.AppSettings["DefaultUrl"]);
            if (!String.IsNullOrEmpty(tmpUrl))
            {
                SetUrl(tmpUrl);
                tmpUrl = "";
            }
            else if (configUrl.urlValid)
            {
                //As we want to be able to toggle the URL, we'll temporarly save the current url to be able to switch back
                tmpUrl = url;
                SetUrl(configUrl.urlString);
            }
            else
            {
                SetUrl("https://www.bing.com");
            }
        }

        private void SetUrl(string newUrl)
        {
            url = newUrl;
            lbl_Url.Content = newUrl;
        }

        private void BrowserSelection_TextInput(object sender, TextCompositionEventArgs e)
        {
            //Not sexy, but have to use if/else if because switch doesn't support variables as cases
            string input = e.Text.ToString();

            if (input == shortcuts["Chrome"])
            {
                StartChrome();
            }
            else if (input == shortcuts["FF"])
            {
                StartFF();
            }
            else if (input == shortcuts["IE"])
            {
                StartIE();
            }
            else if (input == shortcuts["Edge"])
            {
                StartEdge();
            }
            else if (input == shortcuts["InPrivate"])
            {
                ToggleInPrivate();
            }
            else if (input == shortcuts["UseDefaultUrl"])
            {
                ToggleUseDefaultUrl();
            }
        }

        //Enable a custom title
        private void GetTitleFromAppConfig()
        {
            try
            {
                string configTitle = ConfigurationManager.AppSettings["WindowTitle"];
                if (configTitle.Length > 0)
                {
                    Application.Current.MainWindow.Title = configTitle;
                }
            }
            catch (Exception)
            {

            }
        }

        private void ShowDefaultProgramsUI()
        {
            Process.Start("control", "/name Microsoft.DefaultPrograms");
        }

        private void btn_Help_Click(object sender, RoutedEventArgs e)
        {
            ShowHelpWindow();
        }

        private void ShowHelpWindow()
        {
            HelpWindow helpWindow = new HelpWindow();
            helpWindow.Show();
        }

        private string GetKeyboardShortcuts(string task)
        {
            switch (task)
            {
                case "Chrome":
                    return ConfigurationManager.AppSettings["ChromeShortcut"];
                case "FF":
                    return ConfigurationManager.AppSettings["FFShortcut"];
                case "IE":
                    return ConfigurationManager.AppSettings["IEShortcut"];
                case "Edge":
                    return ConfigurationManager.AppSettings["EdgeShortcut"];
                case "InPrivate":
                    return ConfigurationManager.AppSettings["InPrivateShortcut"];
                case "UseDefaultUrl":
                    return ConfigurationManager.AppSettings["ToggleUseDefaultUrl"];
                default:
                    throw new IndexOutOfRangeException("Unknown task for GetKeyboardShortcuts");
            }
        }

        private void SetAppIcon()
        {
            string currentDir = System.IO.Directory.GetCurrentDirectory();
            string iconPath = System.IO.Path.Combine(currentDir, "BrowserChoice.ico");
            if (System.IO.File.Exists(iconPath))
            {
                BitmapImage customIcon = new BitmapImage();
                customIcon.BeginInit();
                customIcon.UriSource = new Uri(iconPath);
                customIcon.EndInit();
                App.Current.MainWindow.Icon = customIcon;
            }
        }

        // Get a list of installed browsers to grey-out non-available options
        private static Dictionary<string, string> GetInstalledBrowsers()
        {
            Dictionary<string, string> browserList = new Dictionary<string, string>();

            RegistryKey browserKeys;

            browserKeys = Registry.LocalMachine.OpenSubKey(@"SOFTWARE\WOW6432Node\Clients\StartMenuInternet");

            if (browserKeys == null)
            {
                browserKeys = Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Clients\StartMenuInternet");
            }

            string[] browserNames = browserKeys.GetSubKeyNames();

            foreach (string browser in browserNames)
            {
                using (RegistryKey tempKey = browserKeys.OpenSubKey(browser))
                {
                    try
                    {
                        string cmdKey = tempKey.OpenSubKey("shell\\open\\command").GetValue(null).ToString();

                        switch (tempKey.GetValue(null).ToString())
                        {

                            case "Internet Explorer":
                                browserList.Add("IE", cmdKey);
                                break;
                            case "Google Chrome":
                                browserList.Add("Chrome", cmdKey);
                                break;
                            case "Mozilla Firefox":
                                browserList.Add("FF", cmdKey);
                                break;
                            default:
                                break;
                        }
                    }
                    catch
                    {
                        //throw;
                    }
                }
            }
            return browserList;
        }

        private bool GetInstalledEdge()
        {
            //if (Environment.OSVersion.Version.Major == 10)
            int majorVersionNumber = Convert.ToInt32(Registry.GetValue("HKEY_LOCAL_MACHINE\\SOFTWARE\\Microsoft\\Windows NT\\CurrentVersion", "CurrentMajorVersionNumber", null));
            if (majorVersionNumber == 10)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private void EnableAvailableBrowsers()
        {
            Dictionary<string, string> browserList = new Dictionary<string, string>();
            browserList = GetInstalledBrowsers();

            foreach (string browser in browserList.Keys)
            {
                switch (browser)
                {
                    case "Chrome":
                        this.btn_Chrome.IsEnabled = true;
                        this.btn_Chrome.Opacity = 100;
                        break;
                    case "FF":
                        this.btn_FF.IsEnabled = true;
                        this.btn_FF.Opacity = 100;
                        break;
                    case "IE":
                        this.btn_IE.IsEnabled = true;
                        this.btn_IE.Opacity = 100;
                        break;
                }
            }

            if (GetInstalledEdge())
            {
                this.btn_Edge.IsEnabled = true;
                this.btn_Edge.Opacity = 100;
            }
        }

        private void lbl_Url_MouseRightButtonDown(object sender, MouseButtonEventArgs e)
        {
            Clipboard.SetText(lbl_Url.Content.ToString());
            ShowSplashInfo("Copied full URL to clipboard");
        }

        private void lbl_Url_MouseEnter(object sender, MouseEventArgs e)
        {
            //lbl_Url.ToolTip = lbl_Url.Content;
        }

        private void ShowSplashInfo(string splashText)
        {
            //lbl_Splash.Content = "Copied full URL to clipboard";
            //lbl_Splash.Visibility = Visibility.Visible;
            //WaitForIt(1000);
            //lbl_Splash.Visibility = Visibility.Hidden;
            lbl_Splash.Content = splashText;

            lbl_Splash.Visibility = Visibility.Visible;

            DispatcherTimer timer = new DispatcherTimer();
            //Set the timer interval to the length of the animation.
            timer.Interval = new TimeSpan(0, 0, 5);
            timer.Tick += (EventHandler)delegate (object snd, EventArgs ea)
            {
                // The animation will be over now, collapse the label.
                lbl_Splash.Visibility = Visibility.Collapsed;
                // Get rid of the timer.
                ((DispatcherTimer)snd).Stop();
            };
            timer.Start();
        }

        // We'll just run the actual update if we enable it in the app.config and the last check is older than 6hours
        private void CheckUpdate()
        {
            try
            {
                if (bool.Parse(ConfigurationManager.AppSettings["UpdateCheck"]))
                {
                    if (Update.TimeForOnlineCheck())
                    {
                        //Uri.TryCreate(ConfigurationManager.AppSettings["UpdateUrl"], UriKind.Absolute, out updateUrl);
                        Update updateCheck = new Update();
                        Version localFile = updateCheck.GetLocalVersion();
                        Version remoteFile = updateCheck.GetRemoteVersion();

                        if (updateCheck.UpdateAvailable(localFile, remoteFile))
                        {
                            ShowSplashInfo("BrowserChoice Update available.");
                        }
                    }
                }
            }
            catch (Exception)
            {

                throw;
            }

        }
    }

    // It's supposed to be easier to handle the various cases by having my own URL class. As I'll pass it as argument
    // to browsers I'll keep it as string instead of a Uri type
    public class URLChoice
    {
        public bool urlValid { get; set; }
        public string urlString { get; set; }
    }
}
