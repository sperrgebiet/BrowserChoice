﻿namespace BrowserChoice
{
    partial class HelpWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(HelpWindow));
            this.splitContainer = new System.Windows.Forms.SplitContainer();
            this.webBrowser = new System.Windows.Forms.WebBrowser();
            this.lbl_ZoomPerct = new System.Windows.Forms.Label();
            this.lbl_Zoom = new System.Windows.Forms.Label();
            this.zoomSlider = new System.Windows.Forms.TrackBar();
            this.lbl_ZoomText = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer)).BeginInit();
            this.splitContainer.Panel1.SuspendLayout();
            this.splitContainer.Panel2.SuspendLayout();
            this.splitContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.zoomSlider)).BeginInit();
            this.SuspendLayout();
            // 
            // splitContainer
            // 
            this.splitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer.IsSplitterFixed = true;
            this.splitContainer.Location = new System.Drawing.Point(0, 0);
            this.splitContainer.Name = "splitContainer";
            this.splitContainer.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer.Panel1
            // 
            this.splitContainer.Panel1.Controls.Add(this.webBrowser);
            // 
            // splitContainer.Panel2
            // 
            this.splitContainer.Panel2.Controls.Add(this.lbl_ZoomText);
            this.splitContainer.Panel2.Controls.Add(this.zoomSlider);
            this.splitContainer.Panel2.Controls.Add(this.lbl_ZoomPerct);
            this.splitContainer.Panel2.Controls.Add(this.lbl_Zoom);
            this.splitContainer.Size = new System.Drawing.Size(1382, 964);
            this.splitContainer.SplitterDistance = 896;
            this.splitContainer.TabIndex = 0;
            // 
            // webBrowser
            // 
            this.webBrowser.AllowWebBrowserDrop = false;
            this.webBrowser.Dock = System.Windows.Forms.DockStyle.Fill;
            this.webBrowser.Location = new System.Drawing.Point(0, 0);
            this.webBrowser.MinimumSize = new System.Drawing.Size(20, 20);
            this.webBrowser.Name = "webBrowser";
            this.webBrowser.Size = new System.Drawing.Size(1382, 896);
            this.webBrowser.TabIndex = 0;
            this.webBrowser.DocumentCompleted += new System.Windows.Forms.WebBrowserDocumentCompletedEventHandler(this.webBrowser_DocumentCompleted);
            // 
            // lbl_ZoomPerct
            // 
            this.lbl_ZoomPerct.AutoSize = true;
            this.lbl_ZoomPerct.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_ZoomPerct.Location = new System.Drawing.Point(530, 15);
            this.lbl_ZoomPerct.Name = "lbl_ZoomPerct";
            this.lbl_ZoomPerct.Size = new System.Drawing.Size(35, 29);
            this.lbl_ZoomPerct.TabIndex = 3;
            this.lbl_ZoomPerct.Text = "%";
            // 
            // lbl_Zoom
            // 
            this.lbl_Zoom.AutoSize = true;
            this.lbl_Zoom.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_Zoom.Location = new System.Drawing.Point(481, 15);
            this.lbl_Zoom.Name = "lbl_Zoom";
            this.lbl_Zoom.Size = new System.Drawing.Size(52, 29);
            this.lbl_Zoom.TabIndex = 2;
            this.lbl_Zoom.Text = "100";
            // 
            // zoomSlider
            // 
            this.zoomSlider.Location = new System.Drawing.Point(562, 13);
            this.zoomSlider.Maximum = 500;
            this.zoomSlider.Minimum = 5;
            this.zoomSlider.Name = "zoomSlider";
            this.zoomSlider.Size = new System.Drawing.Size(487, 80);
            this.zoomSlider.SmallChange = 5;
            this.zoomSlider.TabIndex = 4;
            this.zoomSlider.TickFrequency = 5;
            this.zoomSlider.Value = 100;
            this.zoomSlider.ValueChanged += new System.EventHandler(this.zoomSlider_ValueChanged);
            // 
            // lbl_ZoomText
            // 
            this.lbl_ZoomText.AutoSize = true;
            this.lbl_ZoomText.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_ZoomText.Location = new System.Drawing.Point(411, 15);
            this.lbl_ZoomText.Name = "lbl_ZoomText";
            this.lbl_ZoomText.Size = new System.Drawing.Size(75, 29);
            this.lbl_ZoomText.TabIndex = 5;
            this.lbl_ZoomText.Text = "Zoom";
            // 
            // HelpWindow
            // 
            this.ClientSize = new System.Drawing.Size(1382, 964);
            this.Controls.Add(this.splitContainer);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimizeBox = false;
            this.Name = "HelpWindow";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "BrowserChoice Help";
            this.splitContainer.Panel1.ResumeLayout(false);
            this.splitContainer.Panel2.ResumeLayout(false);
            this.splitContainer.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer)).EndInit();
            this.splitContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.zoomSlider)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer;
        private System.Windows.Forms.WebBrowser webBrowser;
        private System.Windows.Forms.Label lbl_ZoomPerct;
        private System.Windows.Forms.Label lbl_Zoom;
        private System.Windows.Forms.TrackBar zoomSlider;
        private System.Windows.Forms.Label lbl_ZoomText;
    }
}