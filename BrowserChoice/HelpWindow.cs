﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace BrowserChoice
{
    public partial class HelpWindow : Form
    {
        public HelpWindow()
        {
            InitializeComponent();
            string helpFile = Path.Combine(Path.GetDirectoryName(Application.ExecutablePath), "Help.htm");
            webBrowser.Url = new Uri("file:///" + helpFile);
        }

        private void btn_ZoomOut_Click(object sender, EventArgs e)
        {
            SetZoom(Convert.ToInt32(lbl_Zoom.Text) - 10);
        }

        private void btn_ZoomIn_Click(object sender, EventArgs e)
        {
            SetZoom(Convert.ToInt32(lbl_Zoom.Text) + 10);
        }

        private void SetZoom(int zoomFactor)
        {
            webBrowser.Document.Body.Style = "Zoom:" + zoomFactor + "%";
            lbl_Zoom.Text = zoomFactor.ToString();
        }

        private void webBrowser_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
            webBrowser.Document.Body.Style = "Zoom:125%";
            lbl_Zoom.Text = "125";
        }

        private void zoomSlider_ValueChanged(object sender, EventArgs e)
        {
            SetZoom(zoomSlider.Value);
        }
    }
}
