﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Net;
using System.Configuration;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.IO;

namespace BrowserChoice
{
    class Update
    {
        public static bool TimeForOnlineCheck()
        {
            string fileName = Path.Combine(Path.GetTempPath(), "BrowserChoiceUpdate.dat");
            // if the file already exist the chances are high that we did a check before, hence check it first
            if (File.Exists(fileName))
            {
                string fileContent = File.ReadAllText(fileName);
                DateTime lastCheck;
                DateTime.TryParse(fileContent, out lastCheck);
                if (DateTime.Now > lastCheck.AddHours(6))
                {
                    File.WriteAllText(fileName, DateTime.UtcNow.ToLocalTime().ToString());
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                File.WriteAllText(fileName, DateTime.UtcNow.ToLocalTime().ToString());
            }
            return false;
        }

        public Version GetLocalVersion()
        {
            Version localFile = new Version();
            string assemblyPath = System.Reflection.Assembly.GetExecutingAssembly().Location;
            localFile.Full = FileVersionInfo.GetVersionInfo(assemblyPath).FileVersion;
            localFile.Major = FileVersionInfo.GetVersionInfo(assemblyPath).FileMajorPart;
            localFile.Minor = FileVersionInfo.GetVersionInfo(assemblyPath).FileMinorPart;
            localFile.Build = FileVersionInfo.GetVersionInfo(assemblyPath).FileBuildPart;
            return localFile;
        }

        public Version GetRemoteVersion()
        {
            try
            {
                Uri updateUrl;
                Uri.TryCreate(ConfigurationManager.AppSettings["UpdateUrl"], UriKind.Absolute, out updateUrl);
                HttpWebRequest request = WebRequest.Create(updateUrl.AbsoluteUri.ToString()) as HttpWebRequest;
                request.Method = WebRequestMethods.Http.Get;
                request.Accept = "application/json";
                request.ContentType = "application/json; charset=utf-8";
                request.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;

                using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
                {
                    if (response.StatusCode != HttpStatusCode.OK)
                        throw new Exception(String.Format(
                        "Server error (HTTP {0}: {1}).",
                        response.StatusCode,
                        response.StatusDescription));
                    DataContractJsonSerializer jsonSerializer = new DataContractJsonSerializer(typeof(JsonVersion));
                    object objResponse = jsonSerializer.ReadObject(response.GetResponseStream());
                    JsonVersion jsonResult = objResponse as JsonVersion;

                    Version returnObject = new Version();
                    returnObject.Full = jsonResult.LatestVersion;
                    returnObject.Major = jsonResult.LatestMajorVersion;
                    returnObject.Minor = jsonResult.LatestMinorVersion;
                    returnObject.Build = jsonResult.LatestBuild;

                    return returnObject;
                }
            }
            catch (Exception)
            {
                return null;
            }
        }

        public bool UpdateAvailable(Version localFile, Version remoteFile)
        {
            if (remoteFile.Major > localFile.Major)
            {
                return true;
            }
            else if (remoteFile.Minor > localFile.Minor)
            {
                return true;
            }
            else if (remoteFile.Build > localFile.Build)
            {
                return true;
            }
            // If no version is higher we can easily return false so that we know that there is no updated version available
            return false;
        }
    }

    class Version
    {
        public string Full { get; set; }
        public int Major { get; set; }
        public int Minor { get; set; }
        public int Build { get; set; }
    }

    //[DataContract]
    //public class JsonVersion
    //{
    //    [DataMember(Name = "LatestVersion")]
    //    public string LatestVersion { get; set; }

    //    [DataMember(Name = "LatestMajorVersion")]
    //    public int LatestMajorVersion { get; set; }

    //    [DataMember(Name = "LatestMinorVersion")]
    //    public int LatestMinorVersion { get; set; }

    //    [DataMember(Name = "LatestBuild")]
    //    public int LatestBuild { get; set; }
    //}

    [DataContractAttribute(Name = "BrowserChoice")]
    public class JsonVersion
    {
        [DataMember]
        public string LatestVersion { get; set; }

        [DataMember]
        public int LatestMajorVersion { get; set; }

        [DataMember]
        public int LatestMinorVersion { get; set; }

        [DataMember]
        public int LatestBuild { get; set; }
    }
}
